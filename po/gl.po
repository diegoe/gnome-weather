# Galician translations for gnome-weather package.
# Copyright (C) 2012 THE gnome-weather'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-weather package.
# Fran Diéguez <frandieguez@gnome.org>, 2012, 2015.
# Fran Dieguez <frandieguez@gnome.org>, 2012-2019.
msgid ""
msgstr ""
"Project-Id-Version: gnome-weather 0.1\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-weather/issues\n"
"POT-Creation-Date: 2019-02-22 01:46+0000\n"
"PO-Revision-Date: 2019-03-03 21:39+0200\n"
"Last-Translator: Fran Dieguez <frandieguez@gnome.org>\n"
"Language-Team: Galician\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"
"X-Project-Style: gnome\n"

#: data/city.ui:8
msgid "City view"
msgstr "Vista de cidade"

#: data/city.ui:30
msgid "Loading…"
msgstr "Cargando…"

#: data/org.gnome.Weather.appdata.xml.in:5
#: data/org.gnome.Weather.desktop.in.in:4 src/app/main.js:53
#: src/app/window.js:231 src/service/main.js:49
msgid "Weather"
msgstr "Clima"

#: data/org.gnome.Weather.appdata.xml.in:6
#: data/org.gnome.Weather.desktop.in.in:5
msgid "Show weather conditions and forecast"
msgstr "Mostrar as condicións e predicións do tmepo"

#: data/org.gnome.Weather.appdata.xml.in:16
msgid ""
"A small application that allows you to monitor the current weather "
"conditions for your city, or anywhere in the world."
msgstr ""
"Un pequeno aplicativo que lle permitirá facer un seguimento das condicións "
"meteorolóxicas actuais da súa cidade, ou calquera lugar do mundo."

#: data/org.gnome.Weather.appdata.xml.in:20
msgid ""
"It provides access to detailed forecasts, up to 7 days, with hourly details "
"for the current and next day, using various internet services."
msgstr ""
"Fornece acceso a predicións detalladas ate 7 días, con información por hora "
"do día actual e seguinte, usando varios servizos de internet."

#: data/org.gnome.Weather.appdata.xml.in:24
msgid ""
"It also optionally integrates with the GNOME Shell, allowing you see the "
"current conditions of the most recently searched cities by just typing its "
"name in the Activities Overview."
msgstr ""
"Tamén se integra con GNOME Shell o que lle permite ver as condicións "
"meteorolóxicas actuais das cidades máis buscadas recentemente escribindo o "
"seu nome na Vista de actividades."

#: data/org.gnome.Weather.appdata.xml.in:48
msgid "The GNOME Project"
msgstr "O Proxecto GNOME"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/org.gnome.Weather.desktop.in.in:7
msgid "@APP_ID@"
msgstr "@APP_ID@"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Weather.desktop.in.in:13
msgid "Weather;Forecast;"
msgstr "Tempo;Clima;Meteoroloxía;Predición;"

#: data/org.gnome.Weather.desktop.in.in:14
msgid "Allows weather information to be displayed for your location."
msgstr "Permítelle ver información do clima para a súa localización."

#: data/org.gnome.Weather.gschema.xml:6
msgid "Configured cities to show weather for"
msgstr "Configura as cidades para as que mostrar o clima"

#: data/org.gnome.Weather.gschema.xml:7
msgid ""
"The locations shown in the world view of gnome-weather. Each value is a "
"GVariant returned by gweather_location_serialize()."
msgstr ""
"As localizacións mostradas na vista do mundo de gnome-weather. Cada valor é "
"un GVariant devolto por gweather_location_serialize()."

#: data/org.gnome.Weather.gschema.xml:14
msgid "Automatic location"
msgstr "Localización automática"

#: data/org.gnome.Weather.gschema.xml:15
msgid ""
"The automatic location is the value of automatic-location switch which "
"decides whether to fetch current location or not."
msgstr ""
"A localización automática é o valor da configuración automatic-location que "
"decide se obter a localización automaticamente ou non"

#: data/places-popover.ui:44
msgid "Automatic Location"
msgstr "Localización automática"

#: data/places-popover.ui:83
msgid "Locating…"
msgstr "Localizando…"

#: data/places-popover.ui:140
msgid "Search for a city"
msgstr "Buscar por unha cidade"

#: data/places-popover.ui:172
msgid "Viewed Recently"
msgstr "Visto recentemente"

#: data/primary-menu.ui:4
msgid "_Temperature Unit"
msgstr "_Unidade de temperatura"

#: data/primary-menu.ui:6
msgid "_Celsius"
msgstr "_Celsius"

#: data/primary-menu.ui:11
msgid "_Fahrenheit"
msgstr "_Fahrenheit"

#: data/primary-menu.ui:19
msgid "_About Weather"
msgstr "_Sobre Tempo"

#: data/weather-widget.ui:39
msgid "Current conditions"
msgstr "Condicións actuais"

#: data/weather-widget.ui:151
msgid "Today"
msgstr "Hoxe"

#: data/weather-widget.ui:180
msgid "Tomorrow"
msgstr "Mañá"

#: data/window.ui:33
msgid "Places"
msgstr "Lugares"

#: data/window.ui:55
msgid "Refresh"
msgstr "Actualizar"

#: data/window.ui:113
msgid "Search for a location"
msgstr "Buscar unha localización"

#: data/window.ui:129
msgid "To see weather information, enter the name of a city."
msgstr "Para ver información do clima, escriba o nome dunha cidade."

#: src/app/forecast.js:37
msgid "Forecast"
msgstr "Predición meteorolóxica"

#: src/app/forecast.js:113
msgid "Forecast not available"
msgstr "Predición non dispoñíbel"

#. Translators: this is a time format without date used for AM/PM
#: src/app/forecast.js:129
msgid "%l∶%M %p"
msgstr "%l∶%M %p"

#. Translators: this is a time format without date used for 24h mode
#: src/app/forecast.js:132
msgid "%R"
msgstr "%R"

#: src/app/weeklyForecast.js:36
msgid "Weekly Forecast"
msgstr "Predición semanal"

#. Translators: this is the time format for full weekday name according to the current locale
#: src/app/weeklyForecast.js:121
msgid "%A"
msgstr "%A"

#: src/app/window.js:120
msgid "Select Location"
msgstr "Seleccione a localización"

#: src/app/window.js:230
msgid "translator-credits"
msgstr "Fran Dieguez <frandieguez@gnome.org>, 2013-2015."

#: src/app/window.js:232
msgid "A weather application"
msgstr "Un aplicativo do clima"

#: src/app/world.js:39
msgid "World view"
msgstr "Vista mundial"

#. TRANSLATORS: this is the temperature string, minimum and maximum.
#. The two values are already formatted, so it would be something like
#. "7 °C / 19 °C"
#: src/misc/util.js:159
#, javascript-format
msgid "%s / %s"
msgstr "%s / %s"

#. TRANSLATORS: this is the description shown in the overview search
#. It's the current weather conditions followed by the temperature,
#. like "Clear sky, 14 °C"
#: src/service/searchProvider.js:181
#, javascript-format
msgid "%s, %s"
msgstr "%s, %s"

#~ msgid "_New"
#~ msgstr "_Nova"

#~ msgid "About"
#~ msgstr "Sobre"

#~ msgid "Quit"
#~ msgstr "Saír"

#~ msgid "%H:%M"
#~ msgstr "%H:%M"

#~ msgid "Detailed forecast"
#~ msgstr "Predición meteorolóxica detallada"

#~ msgid "Select All"
#~ msgstr "Seleccoinar todo"

#~ msgid "Select None"
#~ msgstr "Deseleccionar todo"

#~ msgid "Click on locations to select them"
#~ msgstr "Prema nas localizacións para seleccionalas"

#~ msgid "New"
#~ msgstr "Novo"

#~ msgid "Back"
#~ msgstr "Atrás"

#~ msgid "Select"
#~ msgstr "Seleccionar"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Delete"
#~ msgstr "Eliminar"

#~ msgid "More…"
#~ msgstr "Máis…"

#~ msgid "Forecast for Tomorrow"
#~ msgstr "Predición para mañá"

#~ msgid "%B %d"
#~ msgstr "%B %d"

#~ msgid "%d selected"
#~ msgid_plural "%d selected"
#~ msgstr[0] "%d seleccionada"
#~ msgstr[1] "%d seleccionadas"

#~ msgid "World Weather"
#~ msgstr "Tempo mundial"

#~ msgid "Monday night"
#~ msgstr "Luns á noite"

#~ msgid "Monday morning"
#~ msgstr "Luns á mañá"

#~ msgid "Monday afternoon"
#~ msgstr "Luns á tarde"

#~ msgid "Monday evening"
#~ msgstr "Luns á noitiña"

#~ msgid "Tuesday night"
#~ msgstr "Martes á noite"

#~ msgid "Tuesday morning"
#~ msgstr "Martes á mañá"

#~ msgid "Tuesday afternoon"
#~ msgstr "Martes á tarde"

#~ msgid "Tuesday evening"
#~ msgstr "Martes á noitiña"

#~ msgid "Wednesday night"
#~ msgstr "Mércores á noite"

#~ msgid "Wednesday morning"
#~ msgstr "Mércores á mañá"

#~ msgid "Wednesday afternoon"
#~ msgstr "Mércores á tarde"

#~ msgid "Wednesday evening"
#~ msgstr "Mércores á noitiña"

#~ msgid "Thursday night"
#~ msgstr "Xoves á noite"

#~ msgid "Thursday morning"
#~ msgstr "Xoves á mañá"

#~ msgid "Thursday afternoon"
#~ msgstr "Xoves á tarde"

#~ msgid "Thursday evening"
#~ msgstr "Xoves á tardiña"

#~ msgid "Friday night"
#~ msgstr "Venres á noite"

#~ msgid "Friday morning"
#~ msgstr "Venres á mañá"

#~ msgid "Friday afternoon"
#~ msgstr "Venres á tarde"

#~ msgid "Friday evening"
#~ msgstr "Venres á tardiña"

#~ msgid "Saturday night"
#~ msgstr "Sábado á noite"

#~ msgid "Saturday morning"
#~ msgstr "Sábado á mañá"

#~ msgid "Saturday afternoon"
#~ msgstr "Sábado á tarde"

#~ msgid "Saturday evening"
#~ msgstr "Sábado á noitiña"

#~ msgid "Sunday night"
#~ msgstr "Domingo á noite"

#~ msgid "Sunday morning"
#~ msgstr "Domingo á mañá"

#~ msgid "Sunday afternoon"
#~ msgstr "Domingo á tarde"

#~ msgid "Sunday evening"
#~ msgstr "Domingo á noitiña"

#~ msgid "Tonight"
#~ msgstr "Esta noite"

#~ msgid "This morning"
#~ msgstr "Esta mañá"

#~ msgid "This afternoon"
#~ msgstr "Esta tarde"

#~ msgid "This evening"
#~ msgstr "Esta noitiña"

#~ msgid "Tomorrow night"
#~ msgstr "Mañá á noite"

#~ msgid "Tomorrow morning"
#~ msgstr "Mañá á mañá"

#~ msgid "Tomorrow afternoon"
#~ msgstr "Mañá á tarde"

#~ msgid "Tomorrow evening"
#~ msgstr "Mañá á noitiña"

#~ msgid "Cities"
#~ msgstr "Cidades"

#~ msgid "Use the <b>New</b> button on the toolbar to add more world locations"
#~ msgstr ""
#~ "Use o botón <b>Nova</b> na barra de ferramentas para engadir máis "
#~ "localizacións mundiais"

#~ msgid "Click on items to select them"
#~ msgstr "Prema nos elementos para seleccionalos"

#~ msgid "Wind: "
#~ msgstr "Vento:"
